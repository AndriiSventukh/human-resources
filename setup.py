import os
from setuptools import find_packages, setup

with open('requirements.txt') as f:
    required = f.read().splitlines()

setup(
    name='department_app',
    version='1.0.0',
    long_description=__doc__,
    url='https://gitlab.com/AndriiSventukh/human-resources',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=required,
)

