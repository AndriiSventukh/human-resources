# human-resources project

It is possible to install this project by using next command sequence:  

$ git clone git@gitlab.com:AndriiSventukh/human-resources.git  
$ cd human-resources  
$ python -m venv env  
$ . env/bin/activate or $ . env/Scripts/activate  
$ pip install -e .  

Then, it's needed to configure DB connection in config.py file and to make migration:  
$ flask db upgrade  

In order to run the app via Waitress WSGI server can be used:  
$ waitress-serve --call 'department_app:create_app'  
or simply:  
$ . wr_run.sh (or $ source wr_run.sh)

In order to run the app via  built-in development server:  
$ FLASK_APP=department_app  
$ FLASK_ENV=development  
$ flask run  
or simply:  
$ . flask_run.sh (or $ source fkask_run.sh)

APP_ENTRY_POINT:  
- "(socket)/hr"  

REST_API_ENTRY_POINT:  
for EMPLOYEES  
- "(socket)/api/employee/" - GET, POST  
It can be used via:  
$ curl (socket)/api/employee  
$ curl (socket)/api/employee -d "name=Robinson Cruso&date=1997-12-02&department_id=1&salary=2700" -X POST -v  

- "(socket)/api/employee/id" - GET, PUT, DELETE  
It can be used via:  
$ curl (socket)/api/employee/1  
$ curl (socket)/api/employee/2 -d "name=John Armstrong&date=1997-12-02&department_id=2&salary=2400" -X PUT -v  
$ curl (socket)/api/employee/2 -X DELETE -v  

for DEPARTMENTS  
- "(socket)/api/department/" - POST, GET,  
- "(socket)/api/department/id" - GET, PUT, DELETE.  

Since department has relationship to employees (model: one-to-many),  
it is allowed to create new employee only if related department accordingly exists.  


## BRIEF FUNCTIONALITY DESCRIPTION  

Current app gives an opportunity to manage company's human resources.  
It contains only two pages with some popping-up on demand modals.  
The first one is the department's view that realizes all necessary functionality  
on creating, editing and removing company's departments:  
![Image](documentation/hr_department.png)  

The next one is employee's view that realizes the same functionality and additionally  
gives an opportunity to filter employees by date of birth:  
![Image](documentation/hr_employee.png)  

It's necessary to remember that department removing ends up in removing all employed staff in it.  
No bench option available in this case.  










