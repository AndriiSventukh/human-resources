from config import DevelopmentConfig
from flask import Flask
from .extensions import db, migrate, api
from . import routes

__all__ = ['routes',
           'extensions']


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_object(DevelopmentConfig())
    else:
        app.config.from_mapping(test_config)

    db.init_app(app)
    migrate.init_app(app, db)

    api.init_app(app)

    app.register_blueprint(routes.view_bp)

    return app
