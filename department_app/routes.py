from flask import Blueprint
from .extensions import api
from .rest.rest import EmployeeAPI, DepartmentAPI

# Rest API with Flask usually supposes using Flask-RESTful. So using resources is common practice
api.add_resource(EmployeeAPI, '/api/employee', '/api/employee/', '/api/employee/<int:id_>')
api.add_resource(DepartmentAPI, '/api/department', '/api/department/', '/api/department/<int:id_>')

# For creating separate program block of views usually blueprints are used
view_bp = Blueprint('hr', __name__, url_prefix='/hr')

# The view_employee, view_department and import here is needed to avid cyclic reference
from department_app.views import view_employees, view_departments

