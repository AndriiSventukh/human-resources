from flask_restful import Resource, reqparse, abort
from ..extensions import db
from flask import jsonify, make_response
from ..models import employee, department

employee_parser = reqparse.RequestParser()
employee_parser.add_argument('id', type=int)
employee_parser.add_argument('name', type=str, help="Employee's name is required!", required=True)
employee_parser.add_argument('date', type=str, help="Employee's date of birth is required!", required=True)
employee_parser.add_argument('department_id', type=int, help="Employee's department_id is required!", required=True)
employee_parser.add_argument('salary', type=float, help="Employee's salary is required!", required=True)


def abort_if_employee_doesnt_exist(employee_inst):
    if not employee_inst:
        abort(404, message='''Such employee doesn't exist''')


def abort_if_department_doesnt_exist(department_id):
    department_inst = department.Department.query.get(department_id)
    if not department_inst:
        abort(404, message='''Such department doesn't exist''')


def abort_if_employee_already_exists(name):
    if employee.Employee.query.filter_by(name=name).first():
        abort(409, message='''Such employee already exists''')


def abort_if_employee_table_is_empty(employee_inst):
    if not employee_inst:
        abort(404, message='''No data available''')


class EmployeeAPI(Resource):
    def post(self):
        employee_data = employee_parser.parse_args()
        abort_if_employee_already_exists(employee_data['name'])
        abort_if_department_doesnt_exist(employee_data['department_id'])
        employee_inst = employee.Employee(name=employee_data['name'],
                                          date=employee_data['date'],
                                          department_id=employee_data['department_id'],
                                          salary=employee_data['salary'])
        db.session.add(employee_inst)
        db.session.commit()
        return make_response({"id": employee_inst.id,
                              "name": employee_inst.name,
                              "date": employee_inst.date,
                              "department_id": employee_inst.department_id,
                              "salary": employee_inst.salary}, 201)

    def get(self, id_=None):
        if id_ is None:
            employee_inst = employee.Employee.query.all()
            abort_if_employee_table_is_empty(employee_inst)
            employee_inst = [{"id": inst.id,
                              "name": inst.name,
                              "date": inst.date,
                              "department_id": inst.department_id,
                              "salary": inst.salary} for inst in employee_inst]
            return make_response(jsonify(employee_inst), 200)
        else:
            employee_inst = employee.Employee.query.get(id_)
            abort_if_employee_doesnt_exist(employee_inst)
            return make_response({"id": employee_inst.id,
                                  "name": employee_inst.name,
                                  "date": employee_inst.date,
                                  "department_id": employee_inst.department_id,
                                  "salary": employee_inst.salary}, 200)

    def put(self, id_=None):
        employee_data = employee_parser.parse_args()
        if (id_ is None) \
                or employee_data['name'] is None \
                or employee_data['date'] is None \
                or employee_data['department_id'] is None \
                or employee_data['salary'] is None:
            abort(400, message='''Incorrect request''')
        abort_if_employee_already_exists(employee_data['name'])
        employee_inst = employee.Employee.query.get(id_)
        employee_inst.name = employee_data['name']
        employee_inst.date = employee_data['date']
        employee_inst.department_id = employee_data['department_id']
        employee_inst.salary = employee_data['salary']
        db.session.commit()
        return make_response({"id": employee_inst.id,
                              "name": employee_inst.name,
                              "date": employee_inst.date,
                              "department_id": employee_inst.department_id,
                              "salary": employee_inst.salary}, 201)

    def delete(self, id_=None):
        employee_inst = employee.Employee.query.get(id_)
        abort_if_employee_doesnt_exist(employee_inst)
        db.session.delete(employee_inst)
        db.session.commit()
        return make_response({"Employee": employee_inst.name,
                              "status": "Successfully removed"}, 200)
