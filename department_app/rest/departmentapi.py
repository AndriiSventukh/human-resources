from flask_restful import Resource, reqparse, abort
from ..extensions import db
from flask import jsonify, make_response
from ..models import department

department_parser = reqparse.RequestParser()
department_parser.add_argument('id', type=int)
department_parser.add_argument('name', type=str, help="Department name is required!", required=True)


def abort_if_department_doesnt_exist(department_inst):
    if not department_inst:
        abort(404, message='''Such department doesn't exist''')


def abort_if_department_already_exists(name):
    if department.Department.query.filter_by(name=name).first():
        abort(409, message='''Such department already exists''')


def abort_if_department_table_is_empty(department_inst):
    if not department_inst:
        abort(404, message='''No data available''')


class DepartmentAPI(Resource):
    def post(self):
        department_data = department_parser.parse_args()
        abort_if_department_already_exists(department_data['name'])
        department_inst = department.Department(name=department_data['name'])
        db.session.add(department_inst)
        db.session.commit()
        return make_response(jsonify({"id": department_inst.id, "name": department_inst.name}), 201)

    def get(self, id_=None):
        if id_ is None:
            department_inst = department.Department.query.all()
            abort_if_department_table_is_empty(department_inst)
            department_inst = [{"id": inst.id, "name": inst.name} for inst in department_inst]
            return make_response(jsonify(department_inst), 200)
        else:
            department_inst = department.Department.query.get(id_)
            abort_if_department_doesnt_exist(department_inst)
            return make_response({"id": department_inst.id, "name": department_inst.name}, 200)

    def put(self, id_=None):
        department_data = department_parser.parse_args()
        if (id_ is None) or department_data['name'] is None:
            abort(400, message='''Incorrect request''')
        abort_if_department_already_exists(department_data['name'])
        department_inst = department.Department.query.get(id_)
        department_inst.name = department_data['name']
        db.session.commit()
        return make_response({"id": department_inst.id, "name": department_inst.name}, 201)

    def delete(self, id_=None):
        department_inst = department.Department.query.get(id_)
        abort_if_department_doesnt_exist(department_inst)
        db.session.delete(department_inst)
        db.session.commit()
        return make_response({"Department": department_inst.name,
                              "status": "Successfully removed"}, 200)
