from flask_restful import reqparse
from flask import flash, render_template, request
from department_app.routes import view_bp
from ..models.department import Department
from ..models.employee import Employee
from ..extensions import db
import datetime

employee_parser = reqparse.RequestParser()
employee_parser.add_argument('id', type=int)
employee_parser.add_argument('name', type=str, help="Employee's name is required!", required=True)
employee_parser.add_argument('date', type=str, help="Employee's date of birth is required!", required=True)
employee_parser.add_argument('department_id', type=int, help="Employee's department_id is required!", required=True)
employee_parser.add_argument('salary', type=float, help="Employee's salary is required!", required=True)


@view_bp.route('/employees', methods=['GET', 'POST'])
def emp_index(department_id=None):
    first_date = request.form.get('first_date')
    last_date = request.form.get('last_date')
    d_id = int(request.form.get('department_id')) if request.form.get('department_id') else 0
    if not first_date:
        first_date = '1900-01-01'
    if not last_date:
        last_date = str(datetime.date.today())
    if not d_id:
        if department_id:
            d_id = department_id

    all_employees = Employee.query.all()

    if request.method == 'POST':
        if d_id:
            all_employees = Employee.query \
                .filter_by(department_id=d_id) \
                .filter(db.func.date(Employee.date) <= last_date) \
                .filter(db.func.date(Employee.date) >= first_date) \
                .all()
        else:
            all_employees = Employee.query \
                .filter(db.func.date(Employee.date) <= last_date) \
                .filter(db.func.date(Employee.date) >= first_date) \
                .all()

    for employee, i in zip(all_employees, range(1, len(all_employees) + 1)):
        employee.index = i

    all_departments = Department.query.all()
    return render_template('employees.html', employees=all_employees, dep_flag=d_id, departments=all_departments)


@view_bp.route('/employee', methods=['POST'])
def emp_insert():
    employee_data = employee_parser.parse_args()
    d_id = int(request.form['department_id'])
    if Employee.query.filter_by(name=employee_data['name']).first():
        flash('Employee already exists')
    else:
        employee_inst = Employee(name=employee_data['name'],
                                 date=employee_data['date'],
                                 department_id=employee_data['department_id'],
                                 salary=employee_data['salary'])
        db.session.add(employee_inst)
        db.session.commit()
        flash('Employee added successfully')
    return emp_index(d_id)


@view_bp.route('/employee/update', methods=['POST'])
def emp_update():
    employee_data = employee_parser.parse_args()
    d_id = int(request.form['department_id'])
    if employee_data['id'] is None or \
            employee_data['name'] is None or\
            employee_data['date'] is None or\
            employee_data['department_id'] is None or\
            employee_data['salary'] is None:
        flash("Incorrect request")
    else:
        employee_inst = Employee.query.get(employee_data['id'])
        employee_inst.name = employee_data['name']
        employee_inst.date = employee_data['date']
        employee_inst.department_id = employee_data['department_id']
        employee_inst.salary = employee_data['salary']
        db.session.commit()
        flash("Employee's info updated successfully")
    return emp_index(d_id)


@view_bp.route('/employee/delete', methods=['POST'])
def emp_delete():
    d_id = int(request.form['department_id'])
    id_ = request.form['id']
    if id is None:
        flash("Incorrect request")
    else:
        employee_inst = Employee.query.get(id_)
        db.session.delete(employee_inst)
        db.session.commit()
        flash("Employee removed successfully")
    return emp_index(d_id)
