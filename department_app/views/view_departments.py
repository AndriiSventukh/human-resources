from flask_restful import reqparse
from flask import flash, redirect, render_template, request, url_for
from department_app.routes import view_bp
from ..models.department import Department
from ..models.employee import Employee
from ..extensions import db

department_parser = reqparse.RequestParser()
department_parser.add_argument('id', type=int)
department_parser.add_argument('name', type=str, help="Department name is required!", required=True)


@view_bp.route('/', methods=['GET'])
def dep_index():
    all_departments = Department.query.all()
    for department, i in zip(all_departments, range(1, len(all_departments) + 1)):
        department.index = i
        employees = Employee.query.filter_by(department_id=department.id).all()
        sum_s = 0
        for employee in employees:
            sum_s += employee.salary
        if len(employees):
            department.avsalary = int(sum_s/len(employees) * 100) / 100
        else:
            department.avsalary = 0
    return render_template('index.html', departments=all_departments)


@view_bp.route('/department', methods=['POST'])
def dep_insert():
    department_data = department_parser.parse_args()
    if department_data['name'] is None:
        flash("Incorrect request")
    elif Department.query.filter_by(name=department_data['name']).first():
        flash('Department already exists')
    else:
        department_inst = Department(name=department_data['name'])
        db.session.add(department_inst)
        db.session.commit()
        flash('Department added successfully')
    return redirect(url_for('hr.dep_index'))


@view_bp.route('/department/update', methods=['POST'])
def dep_update():
    department_data = department_parser.parse_args()
    if (department_data['id'] is None) or (department_data['name'] is None):
        flash("Incorrect request")
    elif Department.query.filter_by(name=department_data['name']).first():
        flash('Department already exists')
    else:
        department_inst = Department.query.get(department_data['id'])
        department_inst.name = department_data['name']
        db.session.commit()
        flash("Department Updated Successfully")
    return redirect(url_for('hr.dep_index'))


@view_bp.route('/department/delete', methods=['POST'])
def dep_delete():
    id_ = request.form['id']
    if id_ is None:
        flash("Incorrect request")
    else:
        department_inst = Department.query.get(id_)
        employee_insts = Employee.query.filter_by(department_id=id_).all()
        for employee_inst in employee_insts:
            db.session.delete(employee_inst)
            db.session.commit()
        db.session.delete(department_inst)
        db.session.commit()
        flash("Department and employees are successfully removed")
    return redirect(url_for('hr.dep_index'))


