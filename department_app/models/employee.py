from department_app.extensions import db


class Employee(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True, nullable=False)
    date = db.Column(db.Date, nullable=False)
    department_id = db.Column(db.Integer, db.ForeignKey('department.id'), nullable=False)
    salary = db.Column(db.Float, nullable=False)

    def __init__(self, name, date, department_id, salary):
        self.name = name
        self.date = date
        self.department_id = department_id
        self.salary = salary
